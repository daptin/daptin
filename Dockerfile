FROM daptin/daptin:travis
COPY schema_daptin_website.yaml /opt/daptin/schema_daptin_website.yaml

EXPOSE 5000
EXPOSE 8130

ENTRYPOINT ["/opt/daptin/daptin", "-runtime", "release", "-port", ":5000", "-db_type", "postgres",  "-database_url_variable", "DATABASE_URL"]
